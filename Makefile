KAFKA_DIR ?= kafka_2.13-2.6.3

#
# zookeeper
#

zk-install:
	helm install my-zookeeper ./helm/zookeeper

zk-upgrade:
	helm upgrade my-zookeeper ./helm/zookeeper

zk-logs:
	tmuxinator start -p ./tmux/tmuxinator-zk.yaml

#
# kafka
#

kafka-install:
	helm install my-kafka ./helm/kafka

kafka-upgrade:
	helm upgrade my-kafka ./helm/kafka

kafka-logs:
	tmuxinator start -p ./tmux/tmuxinator-kafka.yaml

#
# kowl
#

kowl:
	docker run --rm -ti \
		--network host \
		-p 8080:8080 \
		--entrypoint ./kowl \
		-e KAFKA_INTER_BROKER_LISTENER_NAME=DOCKER \
		-e KAFKA_BROKERS=localhost:19092  \
		quay.io/cloudhut/kowl:v1.5.0
#
# utilities
#

setup-topics:
	$(KAFKA_DIR)/bin/kafka-topics.sh --bootstrap-server localhost:19092 --if-not-exists --create --topic topic-3p-1r --partitions 3 --replication-factor 1
	$(KAFKA_DIR)/bin/kafka-topics.sh --bootstrap-server localhost:19092 --if-not-exists --create --topic topic-3p-2r --partitions 3 --replication-factor 2
	$(KAFKA_DIR)/bin/kafka-topics.sh --bootstrap-server localhost:19092 --if-not-exists --create --topic topic-3p-3r --partitions 3 --replication-factor 3

list-topics:
	$(KAFKA_DIR)/bin/kafka-topics.sh --bootstrap-server localhost:19092 --describe

port-forward:
	tmuxinator start -p ./tmux/tmuxinator-port-forward.yaml
